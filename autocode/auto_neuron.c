//this code should generate an overall neuron structure for num_inputs parameter

/*for this, i need to have a top level verilog neuron module to go off of, so I will need another
program to auto-generate that code that connects the PSPGenerate modules to the 
last3blocks module based on num_inputs
*/

/* this program should take in paramters defining how many layers we want and how many neurons
should be in each layer and the generate verilog code based on that

*/



/* VERILOG CODE TEMPLATE

module neuron(wire in1, in2, in3,....inN, outSpike)
	input wire in1, .... inN ; //incoming spikes
	output reg out_spike ;

	wire p1, w1, p2, w2, p3, w3, pN, wN;

	last3blocks last3 (.p1(p1), .w1(w1))

	pspGenerate pspG1(.delayed_spike(in1
	pspGenerate pspGN()

	*/

#include <stdio.h>

int numInputs=4;

void generateNeuron(numberInputs)
{
	printf("module neuron(");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("spike%d, ",i);
		else 
			printf("spike%d, outSpike);\n\n",i);

	}

	printf("\tinput wire ");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("inSpike%d, ",i);
		else 
			printf("inSpike%d;\n", i);

	}

	printf("\toutput reg outSpike;\n");

	//wires for internal connections between pspGenerator modules and last3blocks

	printf("\twire ");

	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("p%d, w%d, ", i, i);
		else 
			printf("p%d, w%d;\n\n", i, i);

	}

	//now instantiate 1 last3blocks module and N pspGenerator modules in the neuron

	printf("\tlast3blocks last3(");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf(".p%d(p%d), .w%d(w%d), ", i, i, i, i);
		else 
			printf(".p%d(p%d), .w%d(w%d), .spike(outSpike);\n", i, i, i, i);

	}

	for (int i =1; i<numberInputs+1; i++)
	{
		printf("\tpspGenerator pspG%d(",i);
		
			printf(".delayed_spike(inSpike%d), .potential(p%d), .weight(w%d));\n ",i,i,i);

	}

	printf("\nendmodule\n");


}

int main(void){
	
	generateNeuron(numInputs);
	return 0;

}

