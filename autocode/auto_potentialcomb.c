/*this code is to automatically generate a verilog neuron module based on number of input neurons...that is, the number of neurons in the previous layer assuming all the neurons 
in each layer are connected to all neurons in the next layer*/

#include <stdio.h>

int numInputs = 4; //this is the paramter to modify to change the code generation


/* WHAT I NEED TO DO:
	- add one pspgenerate module for each input neuron
	-add a connection for each potential and weight from the pspGenerate modules to the potential_comb module...basically I will add it to the input of the last3blocks module


	*/

void generatePotentialComb(int numberInputs)
{
	//module port declaration
	printf("module potential_comb (");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("psp_results%d, weights%d, ", i, i);
		else 
			printf("psp_results%d, weights%d, clk);\n\n", i, i);

	}

	printf("\tinput wire clk;\n\twire[7:0] ");

	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("psp_results%d, weights%d, ", i, i);
		else
			printf("psp_results%d, weights%d ;\n", i, i);
	}

	printf("\toutput reg [7:0] toAddPSP = 8'b00000000;\n");
	printf("\treg [7:0] weighted_PSP;\n");
	printf("\talways@(posedge clk) begin\n");
	printf("\t\tweighted_PSP = 0;\n\t\tweighted_PSP = ");

	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("psp_results%d*weights%d + ", i , i);
		else 
			printf("psp_results%d*weights%d ;\n", i, i);
		
	}
	printf("\t\ttoAddPSP = weighted_PSP ;\n");
	printf("\tend\n\nendmodule\n");
}

int main(void){
	
	generatePotentialComb(numInputs);
	return 0;

}
