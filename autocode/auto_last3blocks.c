// this code is to generate the module for the last3blocks based on num_inputs parameter

#include <stdio.h>

int numInputs = 4;


void generateLast3blocks(numberInputs)
{
	printf("module last3blocks(");

	for(int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("p%d, w%d, ", i, i);
		else 
			printf("p%d, w%d, spike, refractory);\n\n", i, i);		
	}
	printf("\tinput wire [7:0] ");

	for(int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf("p%d, w%d, ", i, i);
		else 
			printf("p%d, w%d;\n", i, i);		
	}

	printf("\toutput wire spike, refractory;\n");
	printf("\twire [7:0] potcomb2new, sum2compare;\n");

	//bind i/o

	printf("\tpotential_comb pot_comb(");

		for(int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			printf(".psp_results%d(p%d), .weights%d(w%d), ", i, i, i, i);
		else 
			printf(".psp_results%d(p%d), .weights%d(w%d)) ;\n", i, i, i, i);
	}

	printf("\taddPSPfeedback add(.new(potcom2new), .out(sum2compare));\n");
	printf("\toutputControl outcontrol(.potential_to_compare(sum2compare), .spike(spike), .refractory(refractory)\n");
	printf("\n\nendmodule\n");


}


int main(void){
	
	generateLast3blocks(numInputs);
	return 0;

}