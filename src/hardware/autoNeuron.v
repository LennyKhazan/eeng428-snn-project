module neuron(inSpike1, inSpike2, inSpike3, inSpike4, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb psp(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
