module outputControl ( fromAdd, clk, output_spike, refract);

input wire [7:0] fromAdd ;
output reg output_spike;
input wire clk;
reg [7:0] threshold;

output reg refract; //refract=1 means neuron is in refractory period and cannout output a spike during this time
reg [7:0] refractory_time;
//reg refract_period;
	initial begin
		threshold = 9;
		refract = 0;
		//refract_period = 30;
		refractory_time = 10;
	end

always@(posedge clk) begin

	if (refract == 0) begin

		if (fromAdd >= threshold) begin 
			output_spike <= 1;
			refract <= 1;
		end 
		else begin
			output_spike <= 0;
			end
	end else begin
		output_spike <= 0 ;
		refractory_time <= refractory_time - 1; //-2 because clock period is 2 time units
		if (refractory_time == 0) begin
			refract <= 0; 
			refractory_time <= 10; //reset
			end
	end

end

endmodule


