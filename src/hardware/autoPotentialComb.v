module potential_comb (spike1, spike2, spike3, spike4, toAddPSP, clk);

	input wire clk;
	reg [7:0] weights1, weights2, weights3, weights4 ;
	input wire spike1, spike2, spike3, spike4 ;
	output reg [7:0] toAddPSP = 5;
	reg [7:0] weighted_PSP;

	always@(posedge clk) begin

		weighted_PSP <= 0;
		weighted_PSP <= spike1*weights1 + spike2*weights2 + spike3*weights3 + spike4*weights4 ;
		toAddPSP <= weighted_PSP ;
	end

endmodule
