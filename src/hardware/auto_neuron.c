//this code should generate an overall neuron structure for num_inputs parameter

/*for this, i need to have a top level verilog neuron module to go off of, so I will need another
program to auto-generate that code that connects the PSPGenerate modules to the 
last3blocks module based on num_inputs
*/

/* this program should take in paramters defining how many layers we want and how many neurons
should be in each layer and the generate verilog code based on that

*/



/* VERILOG CODE TEMPLATE

module neuron(wire in1, in2, in3,....inN, outSpike)
	input wire in1, .... inN ; //incoming spikes
	output reg out_spike ;

	wire p1, w1, p2, w2, p3, w3, pN, wN;

	last3blocks last3 (.p1(p1), .w1(w1))

	pspGenerate pspG1(.delayed_spike(in1
	pspGenerate pspGN()

	*/

#include <stdio.h>

int numInputs=4;

void generateNeuron(numberInputs)
{
	FILE *f ;
	f = fopen("/Users/TylerWhiting/Documents/SeniorFall/Cloud_FPGA/Final_Project_SNN/autoNeuron.v", "w");
	fprintf(f, "module neuron(");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "inSpike%d, ",i);
		else 
			fprintf(f, "inSpike%d, clk, outSpike);\n\n",i);

	}

	fprintf(f, "\tinput wire clk, ");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "inSpike%d, ",i);
		else 
			fprintf(f, "inSpike%d;\n", i);

	}

	fprintf(f, "\toutput wire outSpike;\n");

	//wires for internal connections between pspGenerator modules and last3blocks

	//fprintf(f, "\twire ");

	/*for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "p%d, w%d, ", i, i);
		else 
			fprintf(f, "p%d, w%d;\n\n", i, i);

	}*/

	//now instantiate 1 last3blocks module and N pspGenerator modules in the neuron
	fprintf(f, "\twire [7:0] psp2add;\n");
	fprintf(f, "\tpotential_comb psp(");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, ".spike%d(inSpike%d), ", i, i);
		else 
			fprintf(f, ".spike%d(inSpike%d), .toAddPSP(psp2add), .clk(clk));\n", i, i);

	}

	
		fprintf(f, "\tadd_and_outwPorts addOut(");
		
		fprintf(f ,".fromPSP(psp2add), .spike(outSpike), .clk(clk));\n ");

	fprintf(f, "\nendmodule\n");


}

int main(void){
	
	generateNeuron(numInputs);
	return 0;

}

