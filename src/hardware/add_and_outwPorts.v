module add_and_outwPorts(clk, fromPSP, spike) ;

	input wire clk;
	input wire [7:0] fromPSP;
	output wire spike;

	wire [7:0] sum2fromAdd;
	wire refractory;

	addUnit add (.new(fromPSP), .out(sum2fromAdd), .clk(clk), .refract(refractory));
	outputControl outC(.fromAdd(sum2fromAdd), .output_spike(spike), .clk(clk), .refract(refractory));



/*	initial begin
		clk = 1;
		fromPSP = 6;

		$monitor ("time = %d, fromPSP = %d, spike = %d, sum = %d, refract=%d, refractTime =%d", $time, fromPSP, spike, add.sum, outC.refract, outC.refractory_time);

		#2 fromPSP=10;
		#8 fromPSP=0;
		#8 fromPSP =3;
		#8 fromPSP = 20;
		#10 $finish;
	end

	always
		#1 clk = !clk; */

endmodule