module neuron_tb (clk, spike1, spike2, spike3, spike4, out_spike) ;

//generates spike outputs, clk, need to define weights in the potentialcomb block

//reads in the output spike, can also monitor other paramters for debugging
	output reg clk;
	output reg spike1, spike2, spike3, spike4;
	input wire out_spike;

	neuron n1(.inSpike1(spike1), .inSpike2(spike2), .inSpike3(spike3), .inSpike4(spike4), 
				.clk(clk), .outSpike(out_spike));

	always
		#1 clk = !clk ;

	initial begin
		n1.psp.weights1 = 1; //set weights in PSP module
		n1.psp.weights2 = 2;
		n1.psp.weights3 = 3;
		n1.psp.weights4 = 6;

		spike1 = 0;
		spike2 = 0;
		spike3 = 0;
		spike4 = 0;

		clk = 1;

		$dumpfile("singleNeuronWaveforms.vcd");
		$dumpvars;

		$monitor("%d, spikes = %b,%b,%b,%b, w=%d%d%d%d, newsum=%d, totsum=%d, outSpike=%b, refract=%d", 
			$time, spike1, spike2, spike3, spike4, n1.psp.weights1, n1.psp.weights2,
				n1.psp.weights3, n1.psp.weights4, n1.psp.toAddPSP, n1.addOut.add.out, out_spike, n1.addOut.outC.refractory_time);

		#7 spike1=1; spike2=0; spike3=0; spike4=0; 
		#2 spike1=0; spike2=0; spike3=0; spike4=0 ; //reset spikes
		#12 spike1=1; spike2=0; spike3=1; spike4=0 ;
		#2 spike1=0; spike2=0; spike3=0; spike4=0 ; //reset spikes
		#12 spike1=0; spike2=0; spike3=0; spike4=1 ;
		#2 spike1=0; spike2=0; spike3=0; spike4=0 ; //reset spikes

		/*#2 spike1=1; spike2=1; spike3=1; spike4=1 ; 
		#2 spike1=1; spike2=1; spike3=1; spike4=0 ;
		#2 spike1=0; spike2=0; spike3=1; spike4=1 ;*/
		#16 spike1=1; spike2=1; spike3=1; spike4=1 ;
		#2 spike1=0; spike2=0; spike3=0; spike4=0 ; //reset spikes
		#4 spike1=1; spike2=0; spike3=1; spike4=1;
		#2 spike1=0; spike2=0; spike3=0; spike4=0 ; //reset spikes

		#20 $finish ;

	end


endmodule

