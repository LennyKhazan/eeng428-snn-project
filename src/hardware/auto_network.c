/* this code is for generating the entire neural network based on 
number of layers and number of neurons per layer
*/

/* 
Need to generate hook neurons up so that each neuron in previous
layer is connected to each neuron of the next layer's input

*/

#include <stdio.h>


//maybe I should make a network.v file that i create the 
//network by hand for a very small size to check to make
//sure it works on a small scale.

//might need to include generate neuron function in here that 
//passes as an argument the number of inputs to each neuron,
//and also needs to change the instantiation name of each 
//neuron

//need to have an open the file, then pass the FILE* as an arg to the 
// generateNeuron() function. The function will write to that file,
//then close the file in this main().




void generateNeuron(int numberInputs, int layer, int num, FILE *f)
{
	//FILE *f ;
	//f = fopen("/Users/TylerWhiting/Documents/SeniorFall/Cloud_FPGA/Final_Project_SNN/autoNeuron.v", "w");
	fprintf(f, "module neuron%d_%d(", layer, num);
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "inSpike%d, ",i);
		else 
			fprintf(f, "inSpike%d, clk, outSpike);\n\n",i);

	}

	fprintf(f, "\tinput wire clk, ");
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "inSpike%d, ",i);
		else 
			fprintf(f, "inSpike%d;\n", i);

	}

	fprintf(f, "\toutput wire outSpike;\n");

	//wires for internal connections between pspGenerator modules and last3blocks

	//fprintf(f, "\twire ");

	/*for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "p%d, w%d, ", i, i);
		else 
			fprintf(f, "p%d, w%d;\n\n", i, i);

	}*/

	//now instantiate 1 last3blocks module and N pspGenerator modules in the neuron
	fprintf(f, "\twire [7:0] psp2add;\n");
	fprintf(f, "\tpotential_comb%d_%d psp%d_%d(", numberInputs, layer, layer, num);
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, ".spike%d(inSpike%d), ", i, i);
		else 
			fprintf(f, ".spike%d(inSpike%d), .toAddPSP(psp2add), .clk(clk));\n", i, i);

	}

	
		fprintf(f, "\tadd_and_outwPorts addOut%d_%d(", layer, num);
		
		fprintf(f ,".fromPSP(psp2add), .spike(outSpike), .clk(clk));\n ");

	fprintf(f, "\nendmodule\n");
}

/*this code is to automatically generate a verilog neuron module based on number of input neurons...that is, the number of neurons in the previous layer assuming all the neurons 
in each layer are connected to all neurons in the next layer*/

//this is the paramter to modify to change the code generation


/* WHAT I NEED TO DO:
	- add one pspgenerate module for each input neuron
	-add a connection for each potential and weight from the pspGenerate modules to the potential_comb module...basically I will add it to the input of the last3blocks module
	*/

void generatePotentialComb(int numberInputs, FILE* f, int layer)
{
	
	//module port declaration
  

	fprintf(f, "module potential_comb%d_%d (", numberInputs, layer+1);
	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "spike%d, ", i);
		else 
			fprintf(f, "spike%d, toAddPSP, clk);\n\n", i);

	}

	fprintf(f, "\tinput wire clk;\n\treg [7:0] ");

	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "weights%d, ", i);
		else
			fprintf(f, "weights%d ;\n", i);
	}

	fprintf(f, "\tinput wire ");

	for (int i = 1; i < numberInputs+1; i++)
		{
			if (i < numberInputs)
				fprintf(f, "spike%d, ", i);
			else
				fprintf(f, "spike%d ;\n", i);
		}


	fprintf(f, "\toutput reg [7:0] toAddPSP = 5;\n");
	fprintf(f, "\treg [7:0] weighted_PSP;\n\n");
	fprintf(f, "\talways@(posedge clk) begin\n\n");
	fprintf(f, "\t\tweighted_PSP <= 0;\n\t\tweighted_PSP <= ");

	for (int i = 1; i < numberInputs+1; i++)
	{
		if (i < numberInputs)
			fprintf(f, "spike%d*weights%d + ", i , i);
		else 
			fprintf(f, "spike%d*weights%d ;\n", i, i);
		
	}
	fprintf(f, "\t\ttoAddPSP <= weighted_PSP ;\n");
	//fprintf(f, "\t\t@(negedge clk);\n\t\ttoAddPSP <= 0;\n");
	fprintf(f, "\tend\n\nendmodule\n");
}



//network needs to instantiate each neuron module...then connect output of each previous layer to input 
//of each neuron in the next layer.

/*
module network()
	wire inputlayerSpike1, ..., inputlayerSpiken ; //number of input neurons
	wire outputSpike1, ..., outputSpiken ; //number of output neurons
	wire layer12, layer23, layern-1n ;  //to connect between layers

	neuron1_1 (.inSpike1(inputlayerSpike1)...., .outspike(layer12))  //add in clock.
	neuron1_n ()
	neuronn_1 (.inSpike1(layern-1n), ....., .outSpike(outputspike1))
	neuronn_n (.inSpike1(layern-1n), ....., .outSpike(outputspiken))

endmodule

*/

void generateNetwork(FILE *f, int numberInputs, int numberOutputs)
{
	fprintf(f, "module network(");

	for (int i=1; i<numberInputs+1; i++)
	{
		fprintf(f, "Input%d, ", i);
	}

	for (int i=1; i<numberOutputs+1; i++)
	{
		if (i < numberOutputs)
			fprintf(f, "Output%d, ", i);
		else 
			fprintf(f, "Output%d, clk);\n\n", i); //DO I ALSO NEED TO ADD OUTPUT PORTS FOR THE WEIGHT REGISTERS
	}

	for (int i=1; i<numberInputs+1; i++)
	{
		fprintf(f, "\tinput wire Input%d ;\n ", i);
	}

	fprintf(f, "\n");

	for (int i=1; i<numberOutputs+1; i++)
	{
		fprintf(f, "\toutput wire Output%d ;\n", i );
	}

	fprintf(f, "\n");

	fprintf(f, "\tinput wire clk ;\n");

	//need one wire for every neuron output, then one wire for each neuron in next layer to assign the first wire to.
	//the other wires are then inputs to the next layers neurons.

	fprintf(f, "\n");

	printf("Enter # of layers in Network: ");
	int numberLayers;
	scanf("%d", &numberLayers);
	int layer_size_vector[numberLayers]; 

	for (int i=1; i<numberLayers+1; i++)
	{
		printf("Enter layer %d size: ", i);
		scanf("%d", &layer_size_vector[i-1]);
		

	}

	//generate all interlayer wires (not output layer)

	for (int k=1; k<numberLayers; k++)
	{
		for (int i=1; i<layer_size_vector[k-1]+1; i++)
		{
				fprintf(f, "\twire l%dn%d ;\n", k, i);

			for (int j=1; j<layer_size_vector[k]+1; j++)
			{
				fprintf(f, "\twire l%dn%dl%dn%d ; \n",k,i,k+1,j) ; //creates new wire for every interlayer connection
								//naming convention is layer_neuron#, nextlayer_neuron#	
			}
		}
	}
//Assign wires
	for (int k=1; k<numberLayers; k++)
	{
		for (int i=1; i<layer_size_vector[k-1]+1; i++)
		{
			for (int j=1; j<layer_size_vector[k]+1; j++)
			{
				fprintf(f, "\tassign l%dn%dl%dn%d = l%dn%d ; \n",k,i,k+1,j, k, i) ; 
								
			}
		}
	}
	


	fprintf(f, "\n");

	//now need to instantiate all individual neuron modules and connect their inputs/outputs to wires 
	//instantiated in this network module

	//first layer neurons are connected to inputSpike wires

	for (int i=1; i<layer_size_vector[0]+1; i++)
	{
		fprintf(f, "\tneuron1_%d n1_%d(.outSpike(l1n%d), .clk(clk),  ",i, i, i);

			for(int j=1; j<numberInputs+1; j++)
			{
				if (j<numberInputs)
					fprintf(f, ".inSpike%d(Input%d), ", j, j);
				else 
					fprintf(f, ".inSpike%d(Input%d)) ;\n", j, j);

			}

	}

	//Now for hidden layer neurons instantiation, does not include final layer
	
	for (int i=2; i<numberLayers; i++)  //start at 2nd layer
	{
		for(int j=1; j<layer_size_vector[i-1]+1; j++)
		{
			fprintf(f, "\tneuron%d_%d n%d_%d (.clk(clk), .outSpike(l%dn%d) ",i,j,i,j,i,j);

			for(int k=1; k<layer_size_vector[k-1]+1; k++)
			{
				fprintf(f, ", .inSpike%d(l%dn%dl%dn%d)", k, i-1, k, i, j);
			}

			fprintf(f, ") ;\n");
		}
	}

	//Now instantiate output layer neurons

	for (int i=1; i<numberOutputs+1; i++)
	{
		fprintf(f, "\tneuron%d_%d n%d_%d (.clk(clk), .outSpike(Output%d) ", numberLayers, i, numberLayers, i, i);

		for (int j=1; j<layer_size_vector[numberLayers-2]+1; j++)  //each neuron in previous layer must be connected
		{
			fprintf(f, ", .inSpike%d(l%dn%dl%dn%d)", j, numberLayers-1, j, numberLayers, i);
		}

		fprintf(f, ") ;\n");
	}




	fprintf(f, "\nendmodule \n");

}

int numLayers = 3;
int numInputs = 4;  //this should be the number of inputs to the overall network
int numOutputs = 1;

int main(void)
{
	//first need to generate individual neuron modules and PotentialComb modules in each neuron
	int inputNumber = numInputs ; //store because numInputs will be modified
	int lsize ; 
	FILE *f, *p;
	f = fopen("/Users/TylerWhiting/Documents/SeniorFall/Cloud_FPGA/Final_Project_SNN/autoNeurons.v", "w");
	p = fopen("/Users/TylerWhiting/Documents/SeniorFall/Cloud_FPGA/Final_Project_SNN/autoPotentialCombs.v", "w");
	int repeat = 0;

	for (int i=0; i<numLayers; i++)
	{
		printf("Enter layer%d size:", i+1);
		scanf("%d", &lsize);
		generatePotentialComb(numInputs, p, i);
		
			for (int j=0; j<lsize; j++){
				generateNeuron(numInputs, i+1, j+1, f);
			}
		int firstlayerSize = numInputs ; //store first layer size for creatNetwork() function
		numInputs = lsize ;  //replace with next layer
		

	}

	fclose(f);
	fclose(p);

	//Next create overall network using these neurons (which include Potential_comb modules)

	FILE *n ;
	n = fopen("/Users/TylerWhiting/Documents/SeniorFall/Cloud_FPGA/Final_Project_SNN/autoNetwork.v", "w");
	generateNetwork(n, inputNumber, numOutputs);
	fclose(n);

	return 0;
}