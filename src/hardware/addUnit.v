module addUnit (clk, new, refract, out);
	//psp_comb needs to reset new to 0 after it outputs on the clock
	input wire [7:0] new;
	reg [7:0] previous;
	input wire clk;
	output reg [7:0] out;
	reg [7:0] sum;
	input wire refract; //if refractory period is true, output=min

	reg [7:0] new_value;

	reg [7:0] D  = 1;

	reg [7:0] potential_min ; 

	initial begin
		sum = 5;
		previous = 0;
		new_value =0 ;
		potential_min = 5;
		out = 5;
		//$display("sum=%d, new=%d, previous=%d", sum, new, previous);

	end


	/*always@(new) begin
		$display("                                  here");
		sum <= new + previous ;
		previous <= sum;

	end  */

	/*always@(negedge clk) begin
		sum <= out - D;
		if (sum<potential_min) begin
			sum = potential_min;
		end
		out <= sum;
		previous <= out;

	end */
	

	always@(new) begin

		if (refract == 0) begin

		sum <= new + previous ;

		//$display("							       new=%d, previous=%d", new, previous);
		if (sum<potential_min) begin
			sum = potential_min;
		end
		@(negedge clk);
		out <= sum ;
		previous <= out;
		end else 
			out <= potential_min;
		//$display("out=%d, new=%d, previous=%d", out, new, previous);
		//$display("prev=%d", previous);
	end


	



endmodule





