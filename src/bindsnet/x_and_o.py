import os
import torch
import argparse
import numpy as np
import matplotlib.pyplot as plt

from torchvision import transforms
from tqdm import tqdm
import torch.utils.data as utils

from time import time as t

from bindsnet.datasets import create_torchvision_dataset_wrapper
from bindsnet.encoding import PoissonEncoder
from bindsnet.models import DiehlAndCook2015
from bindsnet.network.monitors import Monitor
from bindsnet.utils import get_square_weights, get_square_assignments
from bindsnet.evaluation import all_activity, proportion_weighting, assign_labels
from bindsnet.learning import PostPre
from bindsnet.network import Network
from bindsnet.network.nodes import Input, SRM0Nodes, LIFNodes
from bindsnet.network.topology import Connection
from bindsnet.analysis.plotting import (
    plot_input,
    plot_spikes,
    plot_weights,
    plot_assignments,
    plot_performance,
    plot_voltages,
)


parser = argparse.ArgumentParser()
parser.add_argument("--seed", type=int, default=0)
parser.add_argument("--n_neurons", type=int, default=9)
parser.add_argument("--n_epochs", type=int, default=100)
parser.add_argument("--n_test", type=int, default=10000)
parser.add_argument("--n_workers", type=int, default=-1)
parser.add_argument("--exc", type=float, default=22.5)
parser.add_argument("--inh", type=float, default=120)
parser.add_argument("--theta_plus", type=float, default=0.05)
parser.add_argument("--time", type=int, default=250)
parser.add_argument("--dt", type=int, default=1.0)
parser.add_argument("--intensity", type=float, default=128)
parser.add_argument("--progress_interval", type=int, default=10)
parser.add_argument("--update_interval", type=int, default=10)
parser.add_argument("--train", dest="train", action="store_true")
parser.add_argument("--test", dest="train", action="store_false")
parser.add_argument("--plot", dest="plot", action="store_true")
parser.add_argument("--gpu", dest="gpu", action="store_true")
parser.set_defaults(plot=False, gpu=False, train=True)

class ThreeLayerNetwork(Network):
    def __init__(self, dt):
        super().__init__(dt)
        self.add_layer(Input(n=9, shape=(1, 3, 3), traces=True, tc_trace=20.0), name='X')
        self.add_layer(SRM0Nodes(n=100, traces=True, tc_trace=20.0), name='a')
        self.add_layer(SRM0Nodes(n=100, traces=True, tc_trace=20.0), name='Y')
        w1 = 0.3 * torch.rand(9, 100)
        self.add_connection(Connection(source=self.layers['X'], target=self.layers['a'], w=w1,
            update_rule=PostPre, nu=(1e-4, 1e-2), reduction=None, wmin=0.0, wmax=1.0, norm=78.4),
            source='X', target='a')
        w2 = 0.3 * torch.rand(100, 100)
        self.add_connection(Connection(source=self.layers['a'], target=self.layers['Y'], w=w2,
            update_rule=PostPre, nu=(1e-4, 1e-2), reduction=None, wmin=0.0, wmax=1.0, norm=78.4),
            source='a', target='Y')

args = parser.parse_args()

seed = args.seed
n_neurons = args.n_neurons
n_epochs = args.n_epochs
n_test = args.n_test
n_workers = args.n_workers
exc = args.exc
inh = args.inh
theta_plus = args.theta_plus
time = args.time
dt = args.dt
intensity = args.intensity
progress_interval = args.progress_interval
update_interval = args.update_interval
train = args.train
plot = args.plot
gpu = args.gpu

# Sets up Gpu use
if gpu:
    torch.cuda.manual_seed_all(seed)
else:
    torch.manual_seed(seed)

# Determines number of workers to use
if n_workers == -1:
    n_workers = gpu * 4 * torch.cuda.device_count()

if not train:
    update_interval = n_test

n_neurons = 100
n_sqrt = int(np.ceil(np.sqrt(n_neurons)))
start_intensity = intensity

# Build network.
network = ThreeLayerNetwork(
    dt=dt,
)

# Directs network to GPU
if gpu:
    network.to("cuda")

# Create dataset
data = [
    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),
]

class CustomTensorDataset(utils.TensorDataset):
    def __init__(self, t1, t2):
        super().__init__(
            torch.stack([torch.Tensor(i * 128) for i in t1]),
            torch.stack([torch.Tensor(i) for i in t2]))

XODataset = create_torchvision_dataset_wrapper(CustomTensorDataset)

training_data, labels = zip(*data)

# Load MNIST data.
dataset = XODataset(
    t1=list(training_data),
    t2=list(labels),
    image_encoder=PoissonEncoder(time=time, dt=dt),
    label_encoder=None
)

# Record spikes during the simulation.
spike_record = torch.zeros(update_interval, time, n_neurons)

# Neuron assignments and spike proportions.
n_classes = 4
assignments = -torch.ones(n_neurons)
proportions = torch.zeros(n_neurons, n_classes)
rates = torch.zeros(n_neurons, n_classes)

# Sequence of accuracy estimates.
accuracy = {"all": [], "proportion": []}

# Voltage recording for excitatory and inhibitory layers.
int_voltage_monitor = Monitor(network.layers["a"], ["v"], time=time)
out_voltage_monitor = Monitor(network.layers["Y"], ["v"], time=time)
network.add_monitor(int_voltage_monitor, name="int_voltage")
network.add_monitor(out_voltage_monitor, name="out_voltage")

# Set up monitors for spikes and voltages
spikes = {}
for layer in set(network.layers):
    spikes[layer] = Monitor(network.layers[layer], state_vars=["s"], time=time)
    network.add_monitor(spikes[layer], name="%s_spikes" % layer)

voltages = {}
for layer in set(network.layers) - {"X"}:
    voltages[layer] = Monitor(network.layers[layer], state_vars=["v"], time=time)
    network.add_monitor(voltages[layer], name="%s_voltages" % layer)

inpt_ims, inpt_axes = None, None
spike_ims, spike_axes = None, None
weights_im = None
assigns_im = None
perf_ax = None
voltage_axes, voltage_ims = None, None

# Train the network.
print("\nBegin training.\n")
start = t()

for epoch in range(n_epochs):
    labels = []

    if epoch % progress_interval == 0:
        print("Progress: %d / %d (%.4f seconds)" % (epoch, n_epochs, t() - start))
        start = t()

    # Create a dataloader to iterate and batch data
    dataloader = torch.utils.data.DataLoader(
        dataset, batch_size=1, shuffle=True, num_workers=n_workers, pin_memory=gpu
    )

    for step, batch in enumerate(tqdm(dataloader)):
        # Get next input sample.
        inpts = {"X": batch["encoded_image"].view(time, 1, 1, 3, 3)}
        if gpu:
            inpts = {k: v.cuda() for k, v in inpts.items()}

        if step % update_interval == 0 and step > 0:
            # Convert the array of labels into a tensor
            label_tensor = torch.tensor(labels)

            # Get network predictions.
            all_activity_pred = all_activity(
                spikes=spike_record, assignments=assignments, n_labels=n_classes
            )
            proportion_pred = proportion_weighting(
                spikes=spike_record,
                assignments=assignments,
                proportions=proportions,
                n_labels=n_classes,
            )

            # Compute network accuracy according to available classification strategies.
            accuracy["all"].append(
                100
                * torch.sum(label_tensor.long() == all_activity_pred).item()
                / len(label_tensor)
            )
            accuracy["proportion"].append(
                100
                * torch.sum(label_tensor.long() == proportion_pred).item()
                / len(label_tensor)
            )

            print(
                "\nAll activity accuracy: %.2f (last), %.2f (average), %.2f (best)"
                % (
                    accuracy["all"][-1],
                    np.mean(accuracy["all"]),
                    np.max(accuracy["all"]),
                )
            )
            print(
                "Proportion weighting accuracy: %.2f (last), %.2f (average), %.2f (best)\n"
                % (
                    accuracy["proportion"][-1],
                    np.mean(accuracy["proportion"]),
                    np.max(accuracy["proportion"]),
                )
            )

            # Assign labels to excitatory layer neurons.
            assignments, proportions, rates = assign_labels(
                spikes=spike_record,
                labels=label_tensor,
                n_labels=n_classes,
                rates=rates,
            )

            labels = []

        labels.append(batch["label"])

        # Run the network on the input.
        network.run(inpts=inpts, time=time, input_time_dim=1)

        # Get voltage recording.
        int_voltages = int_voltage_monitor.get("v")
        inh_voltages = out_voltage_monitor.get("v")

        # Add to spikes recording.
        spike_record[step % update_interval] = spikes["Y"].get("s").squeeze()

        # Optionally plot various simulation information.
        if plot:
            perf_ax = plot_performance(accuracy, ax=perf_ax)
            plt.pause(1e-8)

        network.reset_()  # Reset state variables.

print("Progress: %d / %d (%.4f seconds)" % (epoch + 1, n_epochs, t() - start))
print("Training complete.\n")
