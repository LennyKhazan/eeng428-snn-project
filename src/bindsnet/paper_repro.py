import os
import torch
import argparse
import numpy as np
import matplotlib.pyplot as plt

from torchvision import transforms
from tqdm import tqdm

from time import time as t

from bindsnet.datasets import create_torchvision_dataset_wrapper
import torch.utils.data as utils
from bindsnet.datasets import MNIST
from bindsnet.encoding import PoissonEncoder, RepeatEncoder
from bindsnet.models import DiehlAndCook2015
from bindsnet.network.nodes import Nodes, Input, IFNodes
from bindsnet.network.monitors import Monitor
from bindsnet.network.network import Network
from bindsnet.network.topology import Connection
from bindsnet.utils import get_square_weights, get_square_assignments
from bindsnet.evaluation import all_activity, proportion_weighting, assign_labels
from bindsnet.analysis.plotting import (
    plot_input,
    plot_spikes,
    plot_weights,
    plot_assignments,
    plot_performance,
    plot_voltages,
)

class IFNetwork(Network):
    def __init__(self, dt=1.0):
        super().__init__(dt=dt)
        input_layer = Input(n=9, shape=(1, 1, 3, 3))
        first_layer = IFNodes(n=9)
        second_layer = IFNodes(n=4)
        w1 = torch.eye(9)
        first_conn = Connection(source=input_layer, target=first_layer, w=w1)
        w2 = torch.zeros(9, 4)
        w2[3][0] = 1.0
        w2[4][0] = 1.0
        w2[5][0] = 1.0
        w2[1][1] = 1.0
        w2[4][1] = 1.0
        w2[7][1] = 1.0
        w2[0][2] = 1.0
        w2[4][2] = 1.0
        w2[8][2] = 1.0
        w2[2][3] = 1.0
        w2[4][3] = 1.0
        w2[6][3] = 1.0
        second_conn = Connection(source=first_layer, target=second_layer, w=w2)

        self.add_layer(input_layer, name='X')
        self.add_layer(first_layer, name='intermediate')
        self.add_layer(second_layer, name='Y')
        self.add_connection(first_conn, source='X', target='intermediate')
        self.add_connection(second_conn, source='intermediate', target='Y')

network = IFNetwork()

# Create dataset
data = [
    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),

    (np.array([[0, 0, 0], [1, 1, 1], [0, 0, 0]]), np.array([0])),
    (np.array([[0, 1, 0], [0, 1, 0], [0, 1, 0]]), np.array([1])),
    (np.array([[1, 0, 0], [0, 1, 0], [0, 0, 1]]), np.array([2])),
    (np.array([[0, 0, 1], [0, 1, 0], [1, 0, 0]]), np.array([3])),
]

class CustomTensorDataset(utils.TensorDataset):
    def __init__(self, t1, t2):
        super().__init__(
            torch.stack([torch.Tensor(i) for i in t1]),
            torch.stack([torch.Tensor(i) for i in t2]))

XODataset = create_torchvision_dataset_wrapper(CustomTensorDataset)

training_data, labels = zip(*data)

# Load MNIST data.
dataset = XODataset(
    t1=list(training_data),
    t2=list(labels),
    image_encoder=RepeatEncoder(time=250, dt=1.0),
    label_encoder=None
)

# Voltage recording for excitatory and inhibitory layers.
out_monitor = Monitor(network.layers["Y"], ["s"], time=250)
network.add_monitor(out_monitor, name="out_monitor")

dataloader = torch.utils.data.DataLoader(dataset, batch_size=1, shuffle=True)
for step, batch in enumerate(tqdm(dataloader)):
    inputs = {"X": batch['encoded_image'].view(250, 1, 1, 3, 3)}
    network.run(inpts=inputs, time=250)
    out_spikes = out_monitor.get("s")
    print(out_spikes.sum(dim=0))
    print(batch['label'])
    network.reset_()  # Reset state variables.

