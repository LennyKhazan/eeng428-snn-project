
//for each neuron we need to a PSP generator module for each input connected to it
//need an addition untio to add all of the PSP functions. In the paper they uses a MUX
//and an addUnit.
//then need a control module to output a spike if the threshold is met. This module 
//should also initiate the refractory period so a new spike can not be generated 
//during the refractory period

//CONNECTIONS IN TOP LEVEL MODULE NEURON

//each incoming neuron Spike output connected to the delayed spike input of a pspGenerator module
//outputs of pspGenerator module (potential and weight) are inputs to the potential_comb weight and psp vectors
//output of potential_comp (sum) is input to addUnit. Out of addUnit is connected to the second input of addUnit
//output of addUnit also connected to outputControl module.
//output of outputControl moudle is output of neuron top level module


module neuron (input clk, clk_comps, nInputs[O:num_neurons-1], reset,
			   output spike); //top level

//create connections between all submodules


endmodule


module pspGenerator (input clk, delayed_spike, refract, reset,
					 output potential, weight);                  //there will be one of these for each other input neuron to this neuron

//calculate PSP function values through a lookup table, each neuron has its own PSP function,
//if neuron is in refractory period, a different function is used. Eq 2 in the paper. The refarct input will be set to 1.

endmodule

module potential_comb (input clk, clk_comps, psp_results[0:num_inputs], weights[0:num_inputs], reset,
					  output toAddPSP, end_add);    //this adds the new PSP function sum at every clock cycle to send to the addPSP module to sum with the last PSP sum


//psp_results is a vector of all the results from the pspGenerators potential
//weights[] is a vecotr of all wieghts from previous pspGenerator modules
//need to multiply together at every clock cycle

always @(posedge clk_comps)
	begin
		for (i = 0; i < num_inputs; i=i+1) begin
			weighted_PSP[i] = psp_results[i] * weights[i] ;

		end
		//now sum elements of weighted_PSP[]
		reg toAddPSP[7:0] = 0; //arbitrary make 8 bits
		for (i = 0; i < num_inputs) begin
			toAddPSP = toAddPSP + weighted_PSP[i];
		end


	end



endmodule


module addPSP (input A, B, end_add, clk, clk_comps,
			   output sum);

//find sum of all newPSP sum plus sum at last clock cycle
//A is the sum from the last clock cycle, so the output of this module is fed back to the input
//B is the output of the potential_mux unit for this clock cycle

always @(posedge clk_comps)
	begin
		sum = A + B ;
	end

endmodule

module outputControl (input potential_to_compare, clk, reset,
					  output spike, refractory);

//compare PSP sum to threshold, output spike if necessary
//enter refractory period if necessary, no spike can be generated during this time.
reg spike = 0;
reg refractory = 0;

always @(posedge clk) begin

	if (potential_to_compare > threshold) begin
		spike = 1;
		refractory = 1;
	end

end

endmodule



