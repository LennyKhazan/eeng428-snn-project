
//tb for wrong input 1,2,6

//`timescale 10ns/1ps  //1 MHz frequency = 8 ns period 
//`include "axi4lite_adder.v"
`include "autoNetwork.v"

module network_top (clk, spike1, spike2, spike3, spike4, spike5, spike6, spike7, spike8,
					spike9, out_spike1, out_spike2, out_spike3, out_spike4, out1_countF, 
					out2_countF, out3_countF, out4_countF, start, isDone, inputSpikeVector) ;

	output reg spike1, spike2, spike3, spike4, spike5, spike6, spike7, spike8, spike9;
	input wire out_spike1, out_spike2, out_spike3, out_spike4;
	input wire start;
	output reg clk;
	output reg isDone ;
	input wire[31:0] inputSpikeVector ; //contains input pattern
	output reg[7:0] out1_countF, out2_countF, out3_countF, out4_countF;

	reg [7:0] out1_count, out2_count, out3_count, out4_count;  //accumulate counts
	//input wire [7:0] input_sel ; 

	network n (.clk(clk), .Input1(spike1), .Input2(spike2), .Input3(spike3), 
				.Input4(spike4), .Input5(spike5), .Input6(spike6), .Input7(spike7), 
				.Input8(spike8), .Input9(spike9), .Output1(out_spike1), .Output2(out_spike2),
				.Output3(out_spike3), .Output4(out_spike4));

	

	always
		#1 clk = !clk ;

	initial begin
		n.n1_1.psp1_1.weights1 = 1; //set weights in PSP module
		n.n1_1.psp1_1.weights2 = 0;
		n.n1_1.psp1_1.weights3 = 0;
		n.n1_1.psp1_1.weights4 = 0;
		//Neuron 1_2 weights
		n.n1_1.psp1_1.weights5 = 0; //set weights in PSP module
		n.n1_1.psp1_1.weights6 = 0;
		n.n1_1.psp1_1.weights7 = 0;
		n.n1_1.psp1_1.weights8 = 0;

		n.n1_1.psp1_1.weights9 = 0; //set weights in PSP module
		n.n1_2.psp1_2.weights1 = 0;
		n.n1_2.psp1_2.weights2 = 1;
		n.n1_2.psp1_2.weights3 = 0;
		//Neuron 1_2 weights
		n.n1_2.psp1_2.weights4 = 0; //set weights in PSP module
		n.n1_2.psp1_2.weights5 = 0;
		n.n1_2.psp1_2.weights6 = 0;
		n.n1_2.psp1_2.weights7 = 0;

		n.n1_2.psp1_2.weights8 = 0; //set weights in PSP module
		n.n1_2.psp1_2.weights9 = 0;
		n.n1_3.psp1_3.weights1 = 0;
		n.n1_3.psp1_3.weights2 = 0;
		//Neuron 1_2 weight
		n.n1_3.psp1_3.weights3 = 1; //set weights in PSP module
		n.n1_3.psp1_3.weights4 = 0;
		n.n1_3.psp1_3.weights5 = 0;
		n.n1_3.psp1_3.weights6 = 0;

		n.n1_3.psp1_3.weights7 = 0; //set weights in PSP module
		n.n1_3.psp1_3.weights8 = 0;
		n.n1_3.psp1_3.weights9 = 0;
		n.n1_4.psp1_4.weights1 = 0;
		//Neuron 1_2 weights
		n.n1_4.psp1_4.weights2 = 0; //set weights in PSP module
		n.n1_4.psp1_4.weights3 = 0;
		n.n1_4.psp1_4.weights4 = 1;
		n.n1_4.psp1_4.weights5 = 0;
		n.n1_4.psp1_4.weights6 = 0; //set weights in PSP module
		n.n1_4.psp1_4.weights7 = 0;
		n.n1_4.psp1_4.weights8 = 0;
		n.n1_4.psp1_4.weights9 = 0;
		//Neuron 1_2 weights
		n.n1_5.psp1_5.weights1 = 0; //set weights in PSP module
		n.n1_5.psp1_5.weights2 = 0;
		n.n1_5.psp1_5.weights3 = 0;
		n.n1_5.psp1_5.weights4 = 0;

		n.n1_5.psp1_5.weights5 = 1; //set weights in PSP module
		n.n1_5.psp1_5.weights6 = 0;
		n.n1_5.psp1_5.weights7 = 0;
		n.n1_5.psp1_5.weights8 = 0;
		//Neuron 1_2 weights
		n.n1_5.psp1_5.weights9 = 0; //set weights in PSP module
		n.n1_6.psp1_6.weights1 = 0;
		n.n1_6.psp1_6.weights2 = 0;
		n.n1_6.psp1_6.weights3 = 0;

		n.n1_6.psp1_6.weights4 = 0; //set weights in PSP module
		n.n1_6.psp1_6.weights5 = 0;
		n.n1_6.psp1_6.weights6 = 1;
		n.n1_6.psp1_6.weights7 = 0;
		//Neuron 1_2 weights
		n.n1_6.psp1_6.weights8 = 0; //set weights in PSP module
		n.n1_6.psp1_6.weights9 = 0;
		n.n1_7.psp1_7.weights1 = 0;
		n.n1_7.psp1_7.weights2 = 0;
		n.n1_7.psp1_7.weights3 = 0; //set weights in PSP module
		n.n1_7.psp1_7.weights4 = 0;
		n.n1_7.psp1_7.weights5 = 0;
		n.n1_7.psp1_7.weights6 = 0;
		//Neuron 1_2 weight7
		n.n1_7.psp1_7.weights7 = 1; //set weights in PSP module
		n.n1_7.psp1_7.weights8 = 0;
		n.n1_7.psp1_7.weights9 = 0;
		n.n1_8.psp1_8.weights1 = 0;
		//neuron 2_1 weights
		n.n1_8.psp1_8.weights2 = 0; //set weights in PSP module
		n.n1_8.psp1_8.weights3 = 0;
		//neuon 2_2 weight4
		n.n1_8.psp1_8.weights4 = 0; //set weights in PSP module
		n.n1_8.psp1_8.weights5 = 0;
		//neu8on 3_18wieght6
		n.n1_8.psp1_8.weights6 = 0; //set weights in PSP module
		n.n1_8.psp1_8.weights7 = 0;
		n.n1_8.psp1_8.weights8 = 1;
		n.n1_8.psp1_8.weights9 = 0;
		//Neuron 1_2 weights
		n.n1_9.psp1_9.weights1 = 0; //set weights in PSP module
		n.n1_9.psp1_9.weights2 = 0;
		n.n1_9.psp1_9.weights3 = 0;
		n.n1_9.psp1_9.weights4 = 0;
		//neuron 2_1 weights
		n.n1_9.psp1_9.weights5 = 0; //set weights in PSP module
		n.n1_9.psp1_9.weights6 = 0;
		//neuon 2_2 9eight7
		n.n1_9.psp1_9.weights7 = 0; //set weights in PSP module
		n.n1_9.psp1_9.weights8 = 0;
		n.n1_9.psp1_9.weights9 = 1;

		n.n2_1.psp2_1.weights1 = 1;
		n.n2_1.psp2_1.weights2 = 1;
		n.n2_1.psp2_1.weights3 = 1;
		n.n2_1.psp2_1.weights4 = 0;
		n.n2_1.psp2_1.weights5 = 0;
		n.n2_1.psp2_1.weights6 = 0;
		n.n2_1.psp2_1.weights7 = 0;
		n.n2_1.psp2_1.weights8 = 0;
		n.n2_1.psp2_1.weights9 = 0;

		n.n2_2.psp2_2.weights1 = 1;
		n.n2_2.psp2_2.weights2 = 0;
		n.n2_2.psp2_2.weights3 = 0;
		n.n2_2.psp2_2.weights4 = 0;
		n.n2_2.psp2_2.weights5 = 1;
		n.n2_2.psp2_2.weights6 = 0;
		n.n2_2.psp2_2.weights7 = 0;
		n.n2_2.psp2_2.weights8 = 0;
		n.n2_2.psp2_2.weights9 = 1;

		n.n2_3.psp2_3.weights1 = 0;
		n.n2_3.psp2_3.weights2 = 0;
		n.n2_3.psp2_3.weights3 = 1;
		n.n2_3.psp2_3.weights4 = 0;
		n.n2_3.psp2_3.weights5 = 1;
		n.n2_3.psp2_3.weights6 = 0;
		n.n2_3.psp2_3.weights7 = 1;
		n.n2_3.psp2_3.weights8 = 0;
		n.n2_3.psp2_3.weights9 = 0;

		n.n2_4.psp2_4.weights1 = 1;
		n.n2_4.psp2_4.weights2 = 0;
		n.n2_4.psp2_4.weights3 = 0;
		n.n2_4.psp2_4.weights4 = 1;
		n.n2_4.psp2_4.weights5 = 0;
		n.n2_4.psp2_4.weights6 = 0;
		n.n2_4.psp2_4.weights7 = 1;
		n.n2_4.psp2_4.weights8 = 0;
		n.n2_4.psp2_4.weights9 = 0;
		


		spike1 = 0;
		spike2 = 0;
		spike3 = 0;
		spike4 = 0;
		spike5 = 0;
		spike6 = 0;
		spike7 = 0;
		spike8 = 0;
		spike9 = 0;


		clk = 1;
		isDone = 0;

		out1_count = 0; 
		out2_count = 0;
		out3_count = 0; 
		out4_count = 0;

		out1_countF = 0;  //final counts to send to AXI interface
		out2_countF = 0;
		out3_countF = 0;
		out4_countF = 0;

		//$dumpfile("WrongPatternWaveforms.vcd");
		//$dumpvars;
		/*$monitor("%d, out2_1=%b, out2_2=%b, out2_3=%b, out2_4=%b, 1=%d, 2=%d, 3=%d, 4=%d", 
			$time, n.n2_1.outSpike, n.n2_2.outSpike, n.n2_3.outSpike, n.n2_4.outSpike,
				 out1_count, out2_count, out3_count, out4_count); */
				
		//$monitor ("%d, 2spike1=%d, 2spike2=%d, 2spike3=%d, 2spike4=%d, sum=%d, refract=%d", $time, n2.psp.spike1, n2.psp.spike2, n2.psp.spike3, n2.psp.spike4,  n2.addOut.add.out, n2.addOut.add.refract);
end

	always@(posedge start) begin
		repeat (200) begin
			@(negedge clk)
			spike1=inputSpikeVector[0]; spike2=inputSpikeVector[1]; spike3=inputSpikeVector[2]; spike4 = inputSpikeVector[3]; spike5=inputSpikeVector[4]; 
			spike6=inputSpikeVector[5]; spike7=inputSpikeVector[6]; spike8=inputSpikeVector[7]; spike9=inputSpikeVector[8];
			@(negedge clk)
			 spike1=0; spike2=0; spike3=0; spike4=0 ; spike5=0; spike6=0; spike7=0; spike8=0; spike9=0;  //reset spikes
		end

		out1_countF <= out1_count ;
		out2_countF <= out2_count ;
		out3_countF <= out3_count ; 
		out4_countF <= out4_count ;
		isDone <= 1 ; //delay to make sure all signals propogate, then set isdone high
	end



	always@(posedge out_spike1) begin
		out1_count <= out1_count + 1 ;
	end

	always@(posedge out_spike2) begin
		out2_count <= out2_count + 1 ;
	end

	always@(posedge out_spike3) begin
		out3_count <= out3_count + 1 ;
	end

	always@(posedge out_spike4) begin
		out4_count <= out4_count + 1 ;
	end


	/*always @(posedge isDone) begin
		$display("%d,  out1Count = %d, out2Count = %d, out3Count = %d, out4count = %d",
				$time, out1_count, out2_count, out3_count, out4_count);

	end*/


	

endmodule