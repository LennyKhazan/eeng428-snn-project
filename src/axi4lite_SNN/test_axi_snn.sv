// Amazon FPGA Hardware Development Kit
//
// Copyright 2016 Amazon.com, Inc. or its affiliates. All Rights Reserved.
//
// Licensed under the Amazon Software License (the "License"). You may not use
// this file except in compliance with the License. A copy of the License is
// located at
//
//    http://aws.amazon.com/asl/
//
// or in the "license" file accompanying this file. This file is distributed on
// an "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, express or
// implied. See the License for the specific language governing permissions and
// limitations under the License.

//------------------------------------------------------------------------------
// Description: This test checks the byte swap feature of the hello_world CL. It also checks
// if the upper word of the CL register is written to Vdip
//-------------------------------------------------------------------------------

module test_axi_snn();

import tb_type_defines_pkg::*;
// `include "cl_common_defines.vh" // CL Defines with register addresses

// AXI ID
parameter [5:0] AXI_ID = 6'h0;

logic [31:0] rdata;
logic [15:0] vdip_value;
logic [15:0] vled_value;


   initial begin

      tb.power_up();

      $display ("Writing pattern to address ARG1");
      tb.poke(.addr(32'h0000_0500), .data(32'h0000_0007), .id(AXI_ID), .size(DataSize::UINT32), .intf(AxiPort::PORT_OCL)); // write register
      $display ("Writing to ARG2");
      tb.poke(.addr(32'h0000_0504), .data(32'h0000_0003), .id(AXI_ID), .size(DataSize::UINT32), .intf(AxiPort::PORT_OCL)); // write register

      #8000ns;

      tb.peek(.addr(32'h0000_0508), .data(rdata), .id(AXI_ID), .size(DataSize::UINT32), .intf(AxiPort::PORT_OCL));         // start read & write
      $display ("Reading 0x%x", rdata);

      // tb.poke(.addr(32'h0000_0504), .data(32'hffff_fff0), .id(AXI_ID), .size(DataSize::UINT32), .intf(AxiPort::PORT_OCL)); // write register

      #8000ns;

      tb.peek(.addr(32'h0000_0508), .data(rdata), .id(AXI_ID), .size(DataSize::UINT32), .intf(AxiPort::PORT_OCL));         // start read & write
      $display ("Reading 0x%x", rdata);

      // if (rdata == 32'hEFBE_ADDE) // Check for byte swap in register read
      //   $display ("TEST PASSED");
      // else
      //   $error ("TEST FAILED");

      // tb.peek_ocl(.addr(`VLED_REG_ADDR), .data(rdata));         // start read
      // $display ("Reading 0x%x from address 0x%x", rdata, `VLED_REG_ADDR);

      // if (rdata == 32'h0000_BEEF) // Check for LED register read
      //   $display ("*** TEST PASSED ***");
      // else
      //  $error ("*** TEST FAILED ***");

      // vled_value = tb.get_virtual_led();

      // $display ("value of vled:%0x", vled_value);

      tb.kernel_reset();

      tb.power_down();
      
      $finish;
   end

endmodule // test_hello_world
