`include "axi4lite_SNN_slave.v"

module axi4lite_adder_testbench;

parameter DATA_WIDTH=32;
parameter ADDR_REG_ARG1=32'h0000_0500;
parameter ADDR_REG_ARG2=32'h0000_0504;
parameter ADDR_REG_SUM=32'h0000_0508;
parameter ADDER_DELAY=1000;

reg clk_main_a0;
reg rst_main_n_sync;

// to slave
reg awvalid;
reg [DATA_WIDTH-1:0] awaddr;
reg wvalid;
reg [DATA_WIDTH-1:0] wdata;
reg [3:0] wstrb;
reg bready;
reg arvalid;
reg [DATA_WIDTH-1:0] araddr;
reg rready;

// from slave
wire awready;
wire wready;
wire bvalid;
wire [1:0] bresp;
wire arready;
wire rvalid;
wire [DATA_WIDTH-1:0] rdata;
wire [1:0] rres;





axi4lite_adder slave(.clk_main_a0(clk_main_a0), .rst_main_n_sync(rst_main_n_sync), .awvalid(awvalid), 
                      .awaddr(awaddr), .wvalid(wvalid), .wdata(wdata), .wstrb(wstrb), .bready(bready), 
                      .arvalid(arvalid), .araddr(araddr), .rready(rready), .awready(awready), .wready(wready),
                      .bvalid(bvalid), .bresp(bresp), .arready(arready), .rvalid(rvalid), .rdata(rdata), .rresp(rres));

initial begin
  clk_main_a0 = 0;
  rst_main_n_sync = 1;
  awvalid = 0;
  awaddr = 0;
  wvalid = 0;
  wdata = 0;
  wstrb = 0;
  arvalid = 0;
  araddr = 0;
  rready = 0;
  bready = 0;
end

initial begin
  $dumpfile("trace.vcd");
  $dumpvars;
  $monitor("out1Counts = %d, out2Count = %d, out3Count = %d, out4count = %d",
         rdata[7:0], rdata[15:8], rdata[23:16], rdata[31:24]);

end

always
  #5 clk_main_a0 = !clk_main_a0;

initial begin
  // Reset
  #10 rst_main_n_sync = 0;
  #10 rst_main_n_sync = 1;

  // Write to ARG1
  #10 awvalid = 1; awaddr = ADDR_REG_ARG1; wdata = 9'b00000111; wvalid = 1; wstrb = 15; bready = 1;
  #20 awvalid = 0; awaddr = 0; wdata = 0; wvalid = 0; wstrb = 0; bready = 0;

  // Write to ARG2
  #10 awvalid = 1; awaddr = ADDR_REG_ARG2; wdata = 15; wvalid = 1; wstrb = 15; bready = 1;
  #20 awvalid = 0; awaddr = 0; wdata = 0; wvalid = 0; wstrb = 0; bready = 0;

  #10;

  // Read from SUM (before add finishes)
  #10 arvalid = 1; araddr = ADDR_REG_SUM; rready = 1;
  #20 arvalid = 0; araddr = 0;
  #10 rready = 0;

  #(ADDER_DELAY * 10);
  #10;

  // Read from SUM (after add finishes)
  #10 arvalid = 1; araddr = ADDR_REG_SUM; rready = 1;
  #20 arvalid = 0; araddr = 0;
  #10 rready = 0;

  #10;

  // Write to ARG2 again
  #10 awvalid = 1; awaddr = ADDR_REG_ARG2; wdata = 16; wvalid = 1; wstrb = 15; bready = 1;
  #20 awvalid = 0; awaddr = 0; wdata = 0; wvalid = 0; wstrb = 0; bready = 0;

  #(ADDER_DELAY * 10);
  #10;

  // Read from SUM again
  #10 arvalid = 1; araddr = ADDR_REG_SUM; rready = 1;
  #20 arvalid = 0; araddr = 0;
  #10 arvalid = 0; araddr = 0; rready = 0;
  #10;

  $finish;

end



endmodule
