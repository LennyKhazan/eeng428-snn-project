`include "addUnit.v"
`include "outputControl.v"

module add_and_outwPorts(clk, fromPSP, spike) ;

	input wire clk;
	input wire [7:0] fromPSP;
	output wire spike;

	wire [7:0] sum2fromAdd;
	wire refractory;

	addUnit add (.update(fromPSP), .out(sum2fromAdd), .clk(clk), .refract(refractory));
	outputControl outC(.fromAdd(sum2fromAdd), .output_spike(spike), .clk(clk), .refract(refractory));


endmodule
