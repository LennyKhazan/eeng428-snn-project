`include "network_top.v"

module axi4lite_adder (
  input wire clk_main_a0,
  input wire rst_main_n_sync,

  // inputs to AXI-Lite slave
  input wire awvalid,
  input wire [DATA_WIDTH-1:0] awaddr,
  input wire wvalid,
  input wire [DATA_WIDTH-1:0] wdata,
  input wire [3:0] wstrb,
  input wire bready,
  input wire arvalid,
  input wire [DATA_WIDTH-1:0] araddr,
  input wire rready,



  // outputs from AXI-Lite slave
  output reg awready,
  output reg wready,
  output reg bvalid,
  output reg [1:0] bresp,
  output reg arready,
  output reg rvalid,
  output reg [DATA_WIDTH-1:0] rdata,
  output reg [1:0] rresp
  
);
  
  reg [DATA_WIDTH-1:0] inputSpikeVector;
  wire isDone;
  reg start;
  wire[7:0] out1_countF;
  wire[7:0] out2_countF;
  wire[7:0] out3_countF;
  wire[7:0] out4_countF;


network_top nTop (.start(start), .isDone(isDone), .out1_countF(out1_countF), 
              .out2_countF(out2_countF), .out3_countF(out3_countF), .out4_countF(out4_countF), 
              .inputSpikeVector(inputSpikeVector));

//start = 0; //initial - tell network top mod not ready to start

parameter DATA_WIDTH=32;
parameter ADDR_REG_ARG1=32'h0000_0500;
parameter ADDR_REG_ARG2=32'h0000_0504;
parameter ADDR_REG_SUM=32'h0000_0508;
parameter ADDER_DELAY=1000;

reg [DATA_WIDTH-1:0] arg1;
reg [DATA_WIDTH-1:0] arg2;
reg [DATA_WIDTH-1:0] outcounts;
reg arg2_updated;

always @(posedge clk_main_a0) begin
  if (rst_main_n_sync == 0) begin
    awready <= 0;
    wready <= 0;
    bvalid <= 0;
    bresp <= 0;
    arready <= 0;
    rvalid <= 0;
    rdata <= 0;
    rresp <= 0;

    arg1 <= 0;
    arg2 <= 0;
    outcounts <= 0;
    arg2_updated <= 0;
  end

  // Read state machine
  if (arvalid && ~arready)
    arready <= 1;
  else if (arvalid && arready) begin
    arready <= 0;
    if (araddr == ADDR_REG_ARG1) rdata <= arg1;
    else if (araddr == ADDR_REG_ARG2) rdata <= arg2;
    else if (araddr == ADDR_REG_SUM) rdata <= outcounts;
    rvalid <= 1;
  end else if (rready && rvalid) begin
    rvalid <= 0;
    rdata <= 0;
  end

  // Write state machine
  if (awvalid && wvalid && ~awready && ~wready) begin
    awready <= 1;
    wready <= 1;
  end else if (awvalid && wvalid && awready && wready) begin
    if (awaddr == ADDR_REG_ARG1) arg1 <= wdata;
    else if (awaddr == ADDR_REG_ARG2) begin
      arg2 <= wdata;
      if (wdata > 0) arg2_updated <= 1;
    end
    awready <= 0;
    wready <= 0;
    bresp <= 1;
    bvalid <= 1;
  end else if (bvalid && bready) begin
    bvalid <= 0;
  end
end

// Delayed adder
reg [DATA_WIDTH-1:0] delay_counter;
reg [DATA_WIDTH-1:0] arg1_copy;
reg [DATA_WIDTH-1:0] arg2_copy;

always @(posedge clk_main_a0) begin
  if (delay_counter > 0) begin
    delay_counter -= 1;
    if (delay_counter == 0) begin
    //sum = arg1_copy + arg2_copy;
      inputSpikeVector <= arg1 ;
        start <= 1;  //tell network top module it should start
      @(posedge isDone);
        start <= 0 ;
        outcounts[7:0] <= out1_countF ;
        outcounts[15:8] <= out2_countF ;
        outcounts[23:16] <= out3_countF ;
        outcounts[31:24] <= out4_countF ;
       // toMout1_count <= out1_countF ;
        //toMout2_count <= out2_countF ;
        //toMout3_count <= out3_countF ;
        //toMout4_count <= out4_countF ;


        /*$display("out1Count = %d, out2Count = %d, out3Count = %d, out4count = %d",
          out1_countF, out2_countF, out3_countF, out4_countF); */

    end
  end else if (arg2_updated == 1) begin
    arg2_updated <= 0;
    arg1_copy <= arg1;
    arg2_copy <= arg2;
    delay_counter <= ADDER_DELAY;
  end
end

endmodule
