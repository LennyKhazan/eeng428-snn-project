module potential_comb9_1 (spike1, spike2, spike3, spike4, spike5, spike6, spike7, spike8, spike9, toAddPSP, clk);

	input wire clk;
	reg [7:0] weights1, weights2, weights3, weights4, weights5, weights6, weights7, weights8, weights9 ;
	input wire spike1, spike2, spike3, spike4, spike5, spike6, spike7, spike8, spike9 ;
	output reg [7:0] toAddPSP = 0;
	reg [7:0] weighted_PSP;

	always@(posedge clk) begin

		weighted_PSP <= 0;
		weighted_PSP <= spike1*weights1 + spike2*weights2 + spike3*weights3 + spike4*weights4 + spike5*weights5 + spike6*weights6 + spike7*weights7 + spike8*weights8 + spike9*weights9 ;
		toAddPSP <= weighted_PSP ;
	end

endmodule
module potential_comb9_2 (spike1, spike2, spike3, spike4, spike5, spike6, spike7, spike8, spike9, toAddPSP, clk);

	input wire clk;
	reg [7:0] weights1, weights2, weights3, weights4, weights5, weights6, weights7, weights8, weights9 ;
	input wire spike1, spike2, spike3, spike4, spike5, spike6, spike7, spike8, spike9 ;
	output reg [7:0] toAddPSP = 0;
	reg [7:0] weighted_PSP;

	always@(posedge clk) begin

		weighted_PSP <= 0;
		weighted_PSP <= spike1*weights1 + spike2*weights2 + spike3*weights3 + spike4*weights4 + spike5*weights5 + spike6*weights6 + spike7*weights7 + spike8*weights8 + spike9*weights9 ;
		toAddPSP <= weighted_PSP ;
	end

endmodule
