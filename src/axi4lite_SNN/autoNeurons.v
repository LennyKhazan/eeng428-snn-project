`include "add_and_outwPorts.v"
`include "autoPotentialCombs.v"
module neuron1_1(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_1(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_1(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_2(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_2(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_2(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_3(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_3(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_3(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_4(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_4(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_4(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_5(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_5(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_5(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_6(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_6(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_6(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_7(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_7(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_7(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_8(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_8(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_8(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron1_9(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_1 psp1_9(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut1_9(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron2_1(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_2 psp2_1(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut2_1(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron2_2(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_2 psp2_2(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut2_2(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron2_3(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_2 psp2_3(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut2_3(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
module neuron2_4(inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9, clk, outSpike);

	input wire clk, inSpike1, inSpike2, inSpike3, inSpike4, inSpike5, inSpike6, inSpike7, inSpike8, inSpike9;
	output wire outSpike;
	wire [7:0] psp2add;
	potential_comb9_2 psp2_4(.spike1(inSpike1), .spike2(inSpike2), .spike3(inSpike3), .spike4(inSpike4), .spike5(inSpike5), .spike6(inSpike6), .spike7(inSpike7), .spike8(inSpike8), .spike9(inSpike9), .toAddPSP(psp2add), .clk(clk));
	add_and_outwPorts addOut2_4(.fromPSP(psp2add), .spike(outSpike), .clk(clk));
 
endmodule
