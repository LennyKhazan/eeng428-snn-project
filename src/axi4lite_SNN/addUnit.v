module addUnit (clk, update, refract, out);
	//psp_comb needs to reset update to 0 after it outputs on the clock
	input wire [7:0] update;
	reg [7:0] previous;
	input wire clk;
	output reg [7:0] out;
	reg [7:0] sum;
	input wire refract; //if refractory period is true, output=min

	reg [7:0] new_value;

	reg [7:0] D  = 1;

	reg [7:0] potential_min ; 

	initial begin
		sum = 0;
		previous = 0;
		new_value =0 ;
		potential_min = 0;
		out = 0;

	end


	always@(update) begin

		if (refract == 0) begin

		sum <= update + previous ;
		if (sum<potential_min) begin
			sum = potential_min;
		end
		@(negedge clk);
		out <= sum ;
		previous <= out;
		end else 
			out <= potential_min;
	end

endmodule





