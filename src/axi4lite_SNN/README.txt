Only need to run command:

iverilog axi4lite_SNN_tb.v
./a.out

output shows data in rdata of AXI master(testbench) read from the AXI slave that interfaces with the network. These are output counts for each output layer neuron. 



if you want to compile and run auto_network.c, you will need to change file paths in the code to create files in location of your choice.

Modifications are needed in the network_top.v code, as well as the AXI interface(axi4lite_SNN_slave.v) and AXI testbench (axi4lite_SNN_tb.v) if we want to grow the network to something other than 9:4.

to change input spike pattern in axi4lite_SNN_tb.v, change the value of arg1.